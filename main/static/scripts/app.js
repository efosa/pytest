const {
  Component,
  Fragment,
  PureComponent
} = React

class Results extends PureComponent {
  render() {
    const {
      results,
      showLatLong
    } = this.props

    const block = results.results.map(r => {
      const latLongInfo = showLatLong ? (
        <Fragment>
          <div>Latitude: {r.geometry.location.lat}</div>
          <div>Longitude: {r.geometry.location.lng}</div>
        </Fragment>
      ) : null

      return (
        <div key={r.place_id} className="list-group-item list-group-item-success">
          {latLongInfo}
          <div>Formatted Address: {r.formatted_address}</div>
        </div>
      )
    })

    return (
      <div className="list-group">
        {block}
        {/* <li>
          <pre>
            {JSON.stringify(results, null, 2)}
            {JSON.stringify(location, null, 2)}
          </pre>
        </li> */}
      </div>
    );
  }
}

class GeoCodeForm extends Component {
    constructor () {
      super()
      this.state = {
        addrErr: '',
        addrInput: '',
        addrResult: null,
        distanceErr: '',
        distanceInput: '',
        distanceResult: null,
        geocodeErr: '',
        geocodeInput: '',
        geocodeResult: null
      }
      this.getAddr = this.getAddr.bind(this)
      this.getDistance = this.getDistance.bind(this)
      this.getGeoCode = this.getGeoCode.bind(this)
      this.handleAddrChange = this.handleAddrChange.bind(this)
      this.handleDistanceChange = this.handleDistanceChange.bind(this)
      this.handleLatLongChange = this.handleLatLongChange.bind(this)
      this.queryGeoCodeApi = this.queryGeoCodeApi.bind(this)
    }

    queryGeoCodeApi (lookUpType, value) {
      let url, resultName, errName

      switch (lookUpType) {
        case 'address':
          url = '/api/locbyaddr?address=' + value
          resultName = 'addrResult'
          errName = 'addrErr'
          break
        case 'distance':
          url = '/api/distance?geovals=' + value
          resultName = 'distanceResult'
          errName = 'distanceErr'
          break
        case 'lat_long':
          url = '/api/locbygeocode?latlng=' + value
          resultName = 'geocodeResult'
          errName = 'geocodeErr'
          break
        default:
          throw new Error('invalid geocode query-type')
      }

      this.setState({
        [errName]: '',
        [resultName]: null
      })

      fetch(url)
        .then(response => {
          response.json().then(result => {
            if (response.status === 200) {
              if (result.error_message) {
                this.setState({ [errName]: `API Error: ${result.error_message}` })
              } else {
                this.setState({ [resultName]: result })
              }
            } else {
              this.setState({ [errName]: `API Error: ${result.error}` })
            }
          })
        })
    }
    
    getAddr () {
      this.queryGeoCodeApi('lat_long', this.state.geocodeInput)
    }
    
    getDistance () {
      this.queryGeoCodeApi('distance', this.state.distanceInput)
    }

    getGeoCode () {
      this.queryGeoCodeApi('address', this.state.addrInput)
    }
    

    formatErrorBlock (msg) {
      return (
        <div className="alert alert-danger" role="alert">
          {msg}
        </div>
      )
    }

    handleAddrChange (event) {
      this.setState({ addrInput: event.target.value })
    }

    handleDistanceChange (event) {
      this.setState({ distanceInput: event.target.value })
    }

    handleLatLongChange (event) {
      this.setState({ geocodeInput: event.target.value })
    }

    render() {
      const {
        addrErr,
        addrInput,
        addrResult,
        distanceErr,
        distanceInput,
        distanceResult,
        geocodeErr,
        geocodeInput,
        geocodeResult
      } = this.state

      const addrErrBlock = addrErr ? this.formatErrorBlock(addrErr) : null
      const distanceErrBlock = distanceErr ? this.formatErrorBlock(distanceErr) : null
      const geocodeErrBlock = geocodeErr ? this.formatErrorBlock(geocodeErr) : null

      let addrResultBlock
      if (addrResult) addrResultBlock = <Results results={addrResult} showLatLong />

      let distanceResultBlock
      if (distanceResult) distanceResultBlock = (
        <div className="list-group">
          <div className="list-group-item list-group-item-success">
            <div>{distanceResult.km} <strong>km</strong></div>
            <div>{distanceResult.miles} <strong>miles</strong></div>
          </div>
        </div>
      )

      let geoResultBlock
      if (geocodeResult) geoResultBlock = <Results results={geocodeResult} />

      return (
        <div>
          <section>
            <div className="form-group">
              <label for="addressInput">Get GeoCode</label>
              <input
                type="text"
                className="form-control"
                id="addressInput"
                ariaDescribedby="addrHelp"
                placeholder="Enter address"
                value={addrInput}
                onChange={this.handleAddrChange}
              />
              <small
                id="addrHelp"
                className="form-text text-muted">
                e.g. 20 N El Molino, Pasadena, CA 91101
              </small>
            </div>
            {addrErrBlock}
            {addrResultBlock}
            <button onClick={this.getGeoCode} type="submit" className="btn btn-primary">Look up GeoCode</button>
          </section>
          
          <section>
            <div className="form-group">
              <label for="geocodeInput">Find Address</label>
              <input
                type="text"
                className="form-control"
                id="geocodeInput"
                ariaDescribedby="addrHelp"
                placeholder="Enter latitude/longitude"
                value={geocodeInput}
                onChange={this.handleLatLongChange}
              />
              <small id="addrHelp" className="form-text text-muted">e.g. 40.714224,-73.961452</small>
            </div>
            {geocodeErrBlock}
            {geoResultBlock}
            <button onClick={this.getAddr} type="submit" className="btn btn-primary">Look up address</button>
          </section>

          <section>
            <div className="form-group">
              <label for="distanceInput">Calculate Distance</label>
              <input
                type="text"
                className="form-control"
                id="distanceInput"
                ariaDescribedby="distanceHelp"
                placeholder="Enter lat1, lon1, lat2, lon2"
                value={distanceInput}
                onChange={this.handleDistanceChange}
              />
              <small
                id="distanceHelp"
                className="form-text text-muted">e.g. 53.32055555555556,-1.7297222222222221,53.31861111111111,-1.6997222222222223
              </small>
            </div>
            {distanceErrBlock}
            {distanceResultBlock}
            <button onClick={this.getDistance} type="submit" className="btn btn-primary">Look up distance</button>
          </section>

        </div>
      );
    }
  }

  ReactDOM.render(     
    <GeoCodeForm />,
    document.getElementById('root')        
  );