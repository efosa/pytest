from math import radians, cos, sin, asin, sqrt
import os
import requests
from django.http import JsonResponse


GOOGLE_GEOCODE_API_KEY = os.environ.get('GOOGLE_GEOCODE_API_KEY', 'BAD_KEY')
GOOGLE_GEOCODE_BASE_URL = 'https://maps.googleapis.com/maps/api/geocode/json?'
API_KEY = '&key=' + GOOGLE_GEOCODE_API_KEY
DECIMAL_PRECISION = 4

def getLocationByAddr(request):
    try:
        addr = request.GET.get('address')
        if not addr:
            response = JsonResponse({ 'error': 'Bad Request (400) - [address] was not provided, in the format `street address ...`'})
            response.status_code = 400
            return response
        addrQS = 'address=' + addr
        r = requests.get(GOOGLE_GEOCODE_BASE_URL + addrQS + API_KEY)
        return JsonResponse(r.json())

    except:
        response = JsonResponse({ 'error': 'Server Error Occurred' })
        response.status_code = 500
        return response

def getLocationByGeoCode(request):
    try:
        latLong = request.GET.get('latlng')
        if not latLong:
            response = JsonResponse({ 'error': 'Bad Request (400) - [latlng] was not provided, in the format `lat, long`'})
            response.status_code = 400
            return response
        latLongQS = 'latlng=' + latLong
        r = requests.get(GOOGLE_GEOCODE_BASE_URL + latLongQS + API_KEY)
        return JsonResponse(r.json())

    except:
        response = JsonResponse({ 'error': 'Server Error Occurred' })
        response.status_code = 500
        return response

def getDistance(request):
    try:
        geovals = request.GET.get('geovals')
        if not geovals:
            response = JsonResponse({ 'error': 'Bad Request (400) - [geovals] was not provided, in the format `lat1, long1, lat2, long2`'})
            response.status_code = 400
            return response
    
        # convert comma delimited string into array of floats
        latLongArr = geovals.split(',')
        latLongArr = [float(i) for i in latLongArr]

        lat = latLongArr[0]
        lon = latLongArr[1]
        lat1 = latLongArr[2]
        lon1 = latLongArr[3]

        if len(latLongArr) != 4:
            response = JsonResponse({ 'error': 'Bad Request (400) - Incorrect input format. e.g. `lat,long,lat1,long1`'})
            response.status_code = 400
            return response
        
        distInKm = haversine(lat, lon, lat1, lon1)
        
        # calculate miles
        MILES_IN_KM = 0.621371
        distInMiles = distInKm * MILES_IN_KM if distInKm != None else None

        return JsonResponse({
            'km': round(distInKm, DECIMAL_PRECISION),
            'miles': round(distInMiles, DECIMAL_PRECISION)
        })
    except:
        response = JsonResponse({ 'error': 'Server Error Occurred' })
        response.status_code = 500
        return response

def haversine(lat, lon, lat1, lon1):
    """
    Calculate the great circle distance between two points 
    on the earth (specified in decimal degrees)
    """
    # convert decimal degrees to radians 
    lon, lat, lon1, lat1 = map(radians, [lon, lat, lon1, lat1])

    # haversine formula 
    dlon = lon1 - lon 
    dlat = lat1 - lat 
    a = sin(dlat/2)**2 + cos(lat) * cos(lat1) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a)) 

    # Radius of earth in kilometers is 6371
    km = 6371 * c
    return km
