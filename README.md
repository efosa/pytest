# Python / React Test

## Description

I've opted to use inline babel-transpilation for simplicity, so the project does not require npm or webpack to transpile the jsx code. I've tested the project in Safari, Chrome and Firefox, and it ran without issues.

* View code test @ [pyfstest.herokuapp.com](https://pyfstest.herokuapp.com)

## Installation Instructions (shell)

* clone repo
  * `git clone git@bitbucket.org:efosa/pytest.git`
* Setup Google Geocoding API key in dev environment
  * `export GOOGLE_GEOCODE_API_KEY=*************`
* Install python deps
  * `pipenv install`
* To activate this project's virtualenv, run the following:
  * `$ pipenv shell`
* Start project - `python manage.py runserver`